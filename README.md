# README #

Training materials for digital soil mapping provided by Sydney Soil Schools

### What is this repository for? ###

Some training materials for understanding digital soil mapping and assessment

* Literacy in R

* Continuous soil attribute modelling

* Categorical soil attribute modelling

* Quantification of prediction uncertainties

* DSMART

* Digital Soil Assessment: Enterprise Suitability

### How do I get set up? ###

* Just Download the repository into your working directory

or

* Clone Bitbucket repo:

```r
> git clone https://brendo1001@bitbucket.org/brendo1001/usyd_dsm.git --branch canada2017 --single-branch

```

### Contact ###

* Repo owner or admin: Brendan Malone (brendan.malone@sydney.edu.au)

* [web] http://sydney.edu.au/agriculture/academic_staff/brendan.malone.php

* @sOiLmAlOnE