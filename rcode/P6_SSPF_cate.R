## Categorical soil attribute modeling and mapping
 


##  Model validation of categorical prediction models
## Basic example
library(ithir)


## Generate a confusion matrix
con.mat <- matrix(c(5, 0, 1, 2, 0, 15, 0, 5, 0, 1, 31, 0, 0, 10, 2, 11), nrow = 4, ncol = 4)
rownames(con.mat) <- c("DE", "VE", "CH", "KU")
colnames(con.mat) <- c("DE", "VE", "CH", "KU")
con.mat

## COlumn sums. Calculate total number of observations in each class
colSums(con.mat)

## Row sums. Calculate total number of predictions for each class 
rowSums(con.mat)

## Overall accuracy 
ceiling(sum(diag(con.mat))/sum(colSums(con.mat)) * 100)

## Producers accuracy
ceiling(diag(con.mat)/colSums(con.mat) * 100)

##Users accuracy
ceiling(diag(con.mat)/rowSums(con.mat) * 100)

## Goodness of fit stats including kappa coefficent
goofcat(conf.mat = con.mat, imp=TRUE)



########################################################
## Multinomial logistic regression
library(ithir)
library(sp)
library(raster); library(rasterVis)

# Point data 
data(hvTerronDat)

# Covariates
data(hunterCovariates)


## Covariate data extraction 
names(hvTerronDat)
coordinates(hvTerronDat) <- ~ x + y
DSM_data<- extract(hunterCovariates,hvTerronDat, sp= 1, method = "simple")
DSM_data<- as.data.frame(DSM_data)
str(DSM_data)

## Sift out location where there is missing data
which(!complete.cases(DSM_data))
DSM_data<- DSM_data[complete.cases(DSM_data),]



##Begin model fitting
library(nnet)

set.seed(655)
training <- sample(nrow(DSM_data), 0.70 * nrow(DSM_data))
hv.MNLR <- multinom(terron ~ AACN + Drainage.Index + Light.Insolation + TWI + Gamma.Total.Count, data = DSM_data[training, ])

## Model summary
summary(hv.MNLR)

## #Estimate class probabilities
probs.hv.MNLR <- fitted(hv.MNLR)

## #return top of data frame of probabilites
head(probs.hv.MNLR)

## Most probable terron class
pred.hv.MNLR <- predict(hv.MNLR)
summary(pred.hv.MNLR)

## goodness of fit: calibration data
goofcat(observed = DSM_data$terron[training] , predicted = pred.hv.MNLR )

## Validation
V.pred.hv.MNLR <- predict(hv.MNLR, newdata = DSM_data[-training, ])
goofcat(observed = DSM_data$terron[-training] , predicted = V.pred.hv.MNLR )

## Produce soil mapping
## #class prediction
map.MNLR.c<- predict(hunterCovariates, hv.MNLR, type="class",
              filename="hv_MNLR_class.tif",format="GTiff",
              overwrite=T, datatype="INT2S")

## #class probabilities
map.MNLR.p<- predict(hunterCovariates, hv.MNLR, type="probs",
              index = 1,filename="edge_MNLR_probs1.tif", format="GTiff",
              overwrite=T, datatype="FLT4S")

## Plot map
map.MNLR.c <- as.factor(map.MNLR.c)

## ## Add a land class column to the Raster Attribute Table
rat <- levels(map.MNLR.c)[[1]]
rat[["terron"]] <- c("HVT_001","HVT_002", "HVT_003","HVT_004","HVT_005","HVT_006", "HVT_007", "HVT_008", "HVT_009", "HVT_010", "HVT_011", "HVT_012")
levels(map.MNLR.c) <- rat
 
## ## HEX colors
area_colors <- c("#FF0000", "#38A800", "#73DFFF", "#FFEBAF", "#A8A800", "#0070FF", "#FFA77F", "#7AF5CA", "#D7B09E", "#CCCCCC", "#B4D79E", "#FFFF00")
## #plot
levelplot(map.MNLR.c, col.regions=area_colors, xlab="", ylab="")



#########################################################
## C5 Models
library(C50)
hv.C5 <- C5.0(x = DSM_data[training, c("AACN" , "Drainage.Index" , "Light.Insolation" , "TWI" , "Gamma.Total.Count")],
                y = DSM_data$terron[training], trials = 1, rules = FALSE, control = C5.0Control(CF = 0.95, minCases = 20, earlyStopping = FALSE))

#return the class predictions
predict(hv.C5, newdata = DSM_data[training, ])

# #return the class probabilities
predict(hv.C5, newdata = DSM_data[training, ], type = "prob")

## Goodness of fit
C.pred.hv.C5 <- predict(hv.C5, newdata = DSM_data[training, ])
goofcat(observed = DSM_data$terron[training] , predicted = C.pred.hv.C5 )

## Validation
V.pred.hv.C5 <- predict(hv.C5, newdata = DSM_data[-training, ])
goofcat(observed = DSM_data$terron[-training] , predicted = V.pred.hv.C5 )

## Produce digital maps
#class prediction
map.C5.c<- predict(hunterCovariates, hv.C5, type="class",filename="hv_C5_class.tif",format="GTiff",overwrite=T, datatype="INT2S")

## Make plot
map.C5.c <- as.factor(map.C5.c)
rat <- levels(map.C5.c)[[1]]
rat[["terron"]] <- c("HVT_001", "HVT_003","HVT_004","HVT_005","HVT_006", "HVT_007", "HVT_008", "HVT_010")
levels(map.C5.c) <- rat

area_colors <- c("#FF0000", "#73DFFF", "#FFEBAF", "#A8A800", "#0070FF", "#FFA77F", "#7AF5CA", "#CCCCCC")
levelplot(map.C5.c, col.regions=area_colors, xlab="", ylab="")



#########################################################
## Random forest model
library(randomForest)

hv.RF <- randomForest(terron ~ AACN + Drainage.Index + Light.Insolation + TWI + Gamma.Total.Count, data = DSM_data[training, ], ntree = 500, mtry = 5)

#Output random forest model diognostics
print(hv.RF)
 
## #output relative importance of each covariate
importance(hv.RF)

#Prediction of classes
predict(hv.RF, type = "response", newdata = DSM_data[training, ])
 
## #Class probabilities
predict(hv.RF, type = "prob", newdata = DSM_data[training, ])

## Goodness of fit
C.pred.hv.RF <- predict(hv.RF, newdata = DSM_data[training, ])
goofcat(observed = DSM_data$terron[training] , predicted = C.pred.hv.RF )

## Validation
V.pred.hv.RF <- predict(hv.RF, newdata = DSM_data[-training, ])
goofcat(observed = DSM_data$terron[-training] , predicted = V.pred.hv.RF )

## Produce digital maps
#class prediction
map.RF.c<- predict(hunterCovariates, hv.RF,filename="hv_RF_class.tif",format="GTiff",overwrite=T, datatype="INT2S")
 
map.RF.c <- as.factor(map.RF.c)
rat <- levels(map.RF.c)[[1]]
rat[["terron"]] <- c("HVT_001","HVT_002", "HVT_003","HVT_004","HVT_005","HVT_006", "HVT_007", "HVT_008", "HVT_009", "HVT_010", "HVT_011", "HVT_012")
levels(map.RF.c) <- rat
 
## #plot
area_colors <- c("#FF0000", "#38A800", "#73DFFF", "#FFEBAF", "#A8A800", "#0070FF", "#FFA77F", "#7AF5CA", "#D7B09E", "#CCCCCC", "#B4D79E", "#FFFF00")
levelplot(map.RF.c, col.regions=area_colors, xlab="", ylab="")

